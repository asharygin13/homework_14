#include <iostream>
#include <string>

int main()
{
    // ��������� ����������
    std::string myString = "1234567890";
    std::cout << myString << "\n";

    // ����� ����� ������
    size_t length = myString.length();
    std::cout << length << std::endl;

    // ����� ������� ������� ������
    char firstChar = (length > 0) ? myString[0] : '\0';
    std::cout << firstChar << std::endl;

    // ����� ���������� ������� ������
    char lastChar = (length > 0) ? myString[length - 1] : '\0';
    std::cout << lastChar << std::endl;

    return 0;
}
